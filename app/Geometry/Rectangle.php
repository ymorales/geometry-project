<?php

namespace App\Geometry;


class Rectangle extends Figure
{

	private $width, $length;

	public function setWidth($width)
	{
        $this->width = $width;

	}

	public function getWidth()
	{
		return $this->width;
	}

    public function setlength($length)
    {
        $this->length = $length;

    }

    public function getLength()
    {
        return $this->length;
    }

  //
    public function calculateArea()
    {
        $area = $this->width * $this->length;

        return $area;
    }

    public function calculatePerimeter()
    {
        $perimeter = 2 * ($this->width + $this->length); 
    	return $perimeter;
    }
}
