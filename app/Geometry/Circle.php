<?php

namespace App\Geometry;


class Circle extends Figure
{

	private $radius;
    private $pi;

	public function setRadius($radius)
	{
        $this->radius = $radius;
        $this->pi = pi();
	}

	public function getRadius()
	{
		return $this->radius;
	}

  //
    public function calculateArea()
    {
    	$area = $this->pi * pow($this->radius, 2);
        return $area;
    }

    public function calculatePerimeter()
    {
        $perimeter = 2 * $this->pi * $this->radius;
    	return $perimeter;
    }
}