<?php

namespace App\Geometry;

abstract class Figure
{
    //
    abstract protected function calculateArea();
    abstract protected function calculatePerimeter();
}
