<?php

namespace App\Geometry;


class Triangle extends Figure
{

	private $sideA, $sideB, $sideC;

	public function setSide($side, $sideLength)
	{
        $this->{"side".$side} = $sideLength;

	}

	public function getSide($side)
	{
		return $this->{"side".$side};
	}

  //
    public function calculateArea()
    {
    	$semiperimeter = ($this->getSide("A") + $this->getSide("B") + $this->getSide("C")) / 2;
        $area = sqrt($semiperimeter * ($semiperimeter - $this->getSide("A")) * ($semiperimeter - $this->getSide("B")) * ($semiperimeter - $this->getSide("C")));

        return $area;
    }

    public function calculatePerimeter()
    {
        $perimeter = $this->sideA + $this->sideB + $this->sideC;
    	return $perimeter;
    }
}
