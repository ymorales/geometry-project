<?php

namespace App\Geometry;


class Square extends Figure
{

	private $sideLength;

	public function setSideLength($sideLength)
	{
        $this->sideLength = $sideLength;

	}

	public function getSideLength()
	{
		return $this->sideLength;
	}

  //
    public function calculateArea()
    {
        $area = $this->sideLength * $this->sideLength;

        return $area;
    }

    public function calculatePerimeter()
    {
        $perimeter = 4 * $this->sideLength; 
    	return $perimeter;
    }
}