<?php

namespace App\Business;


class TwoDimensionalGeometryFactory implements GeometryFactory
{
    //
    public function makeGeometricFigure($figureType)
    {
        if (class_exists('App\\Geometry\\' .$figureType)) {
            $newClass = 'App\\Geometry\\' . $figureType;
            return new $newClass();
        }
        else {
            return null;
        }

    }
}
