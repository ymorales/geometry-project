<?php

namespace App\Business;

interface GeometryFactory 
{
    //
    public function makeGeometricFigure($figureType);
}
