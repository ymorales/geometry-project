## HOW TO TEST VIA LARAVEL CONSOLE

```
$git clone https://ymorales@bitbucket.org/ymorales/geometry-project.git
$cd geometry-project
$composer install
$php artisan tinker

>>> $geometryFactory = new App\Business\TwoDimensionalGeometryFactory()
>>> $circle = $geometryFactory->makeGeometricFigure("Circle")
>>> $triangle = $geometryFactory->makeGeometricFigure("Triangle")
>>> $square = $geometryFactory->makeGeometricFigure("Square")
>>> $rectangle = $geometryFactory->makeGeometricFigure("Rectangle")

>>> $circle->setRadius(20)
>>> $circle->getRadius()
>>> $circle->calculateArea()
>>> $circle->calculatePerimeter()

>>> $rectangle->setWidth(10);
>>> $rectangle->setLength(20);
>>> $rectangle->getLength()
>>> $rectangle->getWidth()
>>> $rectangle->calculateArea()
>>> $rectangle->calculatePerimeter()

>>> $square->setSideLength(20)
>>> $square->getSideLength()
>>> $square->calculateArea()
>>> $square->calculatePerimeter()

>>> $triangle->setSide("A", 11);
>>> $triangle->setSide("B", 11);
>>> $triangle->setSide("C", 7.5);
>>> $triangle->getSide("A")
>>> $triangle->getSide("B")
>>> $triangle->getSide("C")

>>> $triangle->calculateArea()
>>> $triangle->calculatePerimeter()
```

## HOW TO RUN PHPUNIT TESTS

```
$vendor/phpunit/phpunit/phpunit

PHPUnit 4.8.26 by Sebastian Bergmann and contributors.

.....

Time: 2.54 seconds, Memory: 8.00MB

OK (5 tests, 40 assertions)
```