<?php

use App\Business\TwoDimensionalGeometryFactory;

class TwoDimensionalGeometryFactoryTest extends PHPUnit_Framework_TestCase
{

	public function testTwoDimensionalFactory()
	{
		$geometryFactory = new TwoDimensionalGeometryFactory();

		$this->assertNull($geometryFactory->makeGeometricFigure("Hexagon"));
		$this->assertNull($geometryFactory->makeGeometricFigure("Sphere"));

		//Testing the right creation of circle objects
		$this->assertInstanceOf('App\Geometry\Circle', $geometryFactory->makeGeometricFigure("Circle"));
		$this->assertInstanceOf('App\Geometry\Circle', $geometryFactory->makeGeometricFigure("circle"));

		//Testing the right creation of square objects
		$this->assertInstanceOf('App\Geometry\Square', $geometryFactory->makeGeometricFigure("Square"));
		$this->assertInstanceOf('App\Geometry\Square', $geometryFactory->makeGeometricFigure("square"));

		//Testing the right creation of rectangle objects
		$this->assertInstanceOf('App\Geometry\Rectangle', $geometryFactory->makeGeometricFigure("Rectangle"));
		$this->assertInstanceOf('App\Geometry\Rectangle', $geometryFactory->makeGeometricFigure("rectangle"));

		//Testing the right creation of triangle objects
		$this->assertInstanceOf('App\Geometry\Triangle', $geometryFactory->makeGeometricFigure("Triangle"));
		$this->assertInstanceOf('App\Geometry\Triangle', $geometryFactory->makeGeometricFigure("triangle"));
	}

}