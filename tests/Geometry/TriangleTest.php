<?php

use App\Business\TwoDimensionalGeometryFactory;


class TriangleTest extends PHPUnit_Framework_TestCase
{
	public function testTriangle()
	{
		$geometryFactory = new TwoDimensionalGeometryFactory();
		$triangle = $geometryFactory->makeGeometricFigure("triangle");

		$this->assertNull($triangle->getSide("A"));
		$this->assertNull($triangle->getSide("B"));
		$this->assertNull($triangle->getSide("C"));
		$this->assertEquals(0.0, $triangle->calculateArea());
		$this->assertEquals(0, $triangle->calculatePerimeter());

		$triangle->setSide("A", 11);
		$triangle->setSide("B", 11);
		$triangle->setSide("C", 7.5);
		$this->assertEquals(11 , $triangle->getSide("A"));
		$this->assertEquals(11 , $triangle->getSide("B"));
		$this->assertEquals(7.5 , $triangle->getSide("C"));
		$this->assertEquals(38.7789710249  , $triangle->calculateArea());
		$this->assertEquals(29.5 , $triangle->calculatePerimeter());
	}
	
}