<?php

use App\Business\TwoDimensionalGeometryFactory;
use App\Geometry\Circle;


class CircleTest extends PHPUnit_Framework_TestCase
{
	public function testCircle()
	{
		$geometryFactory = new TwoDimensionalGeometryFactory();
		$circle = $geometryFactory->makeGeometricFigure("circle");

		$this->assertNull($circle->getRadius());
		$this->assertEquals(0, $circle->calculateArea());
		$this->assertEquals(0, $circle->calculatePerimeter());

		$circle->setRadius(20);
		$this->assertEquals(20, $circle->getRadius());
		$this->assertEquals(1256.6370614359, $circle->calculateArea());
		$this->assertEquals(125.66370614359, $circle->calculatePerimeter());
	}
	
}