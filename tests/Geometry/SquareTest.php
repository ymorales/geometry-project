<?php

use App\Business\TwoDimensionalGeometryFactory;


class SquareTest extends PHPUnit_Framework_TestCase
{
	public function testSquare()
	{
		$geometryFactory = new TwoDimensionalGeometryFactory();
		$square = $geometryFactory->makeGeometricFigure("square");

		$this->assertNull($square->getSideLength());
		$this->assertEquals(0, $square->calculateArea());
		$this->assertEquals(0, $square->calculatePerimeter());

		$square->setSideLength(20);
		$this->assertEquals(20 , $square->getSideLength());
		$this->assertEquals(400, $square->calculateArea());
		$this->assertEquals(80 , $square->calculatePerimeter());
	}
	
}