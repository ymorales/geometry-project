<?php

use App\Business\TwoDimensionalGeometryFactory;


class RectangleTest extends PHPUnit_Framework_TestCase
{
	public function testRectangle()
	{
		$geometryFactory = new TwoDimensionalGeometryFactory();
		$rectangle = $geometryFactory->makeGeometricFigure("rectangle");

		$this->assertNull($rectangle->getWidth());
		$this->assertNull($rectangle->getLength());
		$this->assertEquals(0, $rectangle->calculateArea());
		$this->assertEquals(0, $rectangle->calculatePerimeter());

		$rectangle->setWidth(10);
		$rectangle->setLength(20);
		$this->assertEquals(20 , $rectangle->getLength());
		$this->assertEquals(10 , $rectangle->getWidth());
		$this->assertEquals(200, $rectangle->calculateArea());
		$this->assertEquals(60 , $rectangle->calculatePerimeter());
	}
	
}